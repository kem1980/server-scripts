#!/bin/sh

# START SCRIPT
echo "**************************************************"
echo ""
echo "Installing Laravel LEMP Server on Ubuntu 20.04"
echo ""
echo "Beginning Script in date time : $(date)"
echo ""
echo "**************************************************"

# INSTALLING BASE PACKAGES
echo "****************************"
echo "* INSTALLING BASE PACKAGES *"
echo "****************************"
apt update
apt install -y curl ca-certificates zip unzip git supervisor sqlite3 libcap2-bin libpng-dev python2 nano nginx cron certbot python3-certbot-nginx mysql-server phpmyadmin
apt install -y php8.1-cli php8.1-fpm php8.1-dev php8.1-pgsql php8.1-sqlite3 php8.1-gd php8.1-curl php8.1-memcached php8.1-imap php8.1-mysql php8.1-mbstring php8.1-xml php8.1-zip php8.1-bcmath php8.1-soap php8.1-intl php8.1-readline php8.1-pcov php8.1-msgpack php8.1-igbinary php8.1-ldap php8.1-redis
php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer
curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt install -y nodejs
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list

# SSH, HTTP and HTTPS
echo "****************************"
echo "* SET SSH, HTTP and HTTPS  *"
echo "****************************"
ufw allow 22
ufw allow 80
ufw allow 443
ufw --force enable

# UPDATE PHP CLI CONFIGURATION
echo "********************************"
echo "* UPDATE PHP CLI CONFIGURATION *"
echo "********************************"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/8.1/cli/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php/8.1/cli/php.ini
sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php/8.1/cli/php.ini

# UPDATE PHP-FPM SETTINGS
echo "***************************"
echo "* UPDATE PHP-FPM SETTINGS *"
echo "***************************"
sed -i "s/error_reporting = .*/error_reporting = E_ALL \& ~E_NOTICE \& ~E_STRICT \& ~E_DEPRECATED/" /etc/php/8.1/fpm/php.ini
sed -i "s/display_errors = .*/display_errors = Off/" /etc/php/8.1/fpm/php.ini
sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php/8.1/fpm/php.ini
sed -i "s/upload_max_filesize = .*/upload_max_filesize = 256M/" /etc/php/8.1/fpm/php.ini
sed -i "s/post_max_size = .*/post_max_size = 256M/" /etc/php/8.1/fpm/php.ini
sed -i "s/max_execution_time = .*/max_execution_time = 600/" /etc/php/8.1/fpm/php.ini
sed -i "s/default_socket_timeout = .*/default_socket_timeout = 600/" /etc/php/8.1/fpm/php.ini

# SET DOMAIN NAME
echo "Enter domain if exists:"
read DOMAIN_NAME

NGINX_SET_DOMAIN='# server_name example.com;'

if [ $DOMAIN_NAME ]
then
    NGINX_SET_DOMAIN="server_name $DOMAIN_NAME;"
fi

# CREATE INDEX FILE
echo "*********************"
echo "* CREATE INDEX FILE *"
echo "*********************"
mkdir /var/www && rm -rf /var/www/html
cat > /var/www/index.php <<EOF
<?php echo 'php: ' . phpversion(); ?>
EOF

# UPDATE NGINX WEB SERVER
echo "***************************"
echo "* UPDATE NGINX WEB SERVER *"
echo "***************************"
ufw allow 'Nginx Full'
cat > /etc/nginx/sites-available/site.conf <<EOF
server {
    listen 80;

    $NGINX_SET_DOMAIN

    root /var/www;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    index index.php index.html;

    charset utf-8;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
        try_files \$uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_param PATH_INFO \$fastcgi_path_info;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}
EOF

cd /etc/nginx/sites-enabled
rm -rf default
ln -s ../sites-available/site.conf .
cd ~

# TWEAK NGINX SETTINGS
sed -i "s/worker_processes.*/worker_processes auto;/" /etc/nginx/nginx.conf
sed -i "s/# multi_accept.*/multi_accept on;/" /etc/nginx/nginx.conf
sed -i "s/# server_names_hash_bucket_size.*/server_names_hash_bucket_size 128;/" /etc/nginx/nginx.conf
sed -i "s/# server_tokens off/server_tokens off/" /etc/nginx/nginx.conf

# CONFIGURE GZIP FOR NGINX
cat > /etc/nginx/conf.d/gzip.conf <<EOF
gzip_comp_level 5;
gzip_min_length 256;
gzip_proxied any;
gzip_vary on;
gzip_types
application/atom+xml
application/javascript
application/json
application/rss+xml
application/vnd.ms-fontobject
application/x-web-app-manifest+json
application/xhtml+xml
application/xml
font/otf
font/ttf
image/svg+xml
image/x-icon
text/css
text/plain;
EOF

usermod -d /var/lib/mysql/ mysql

service apache2 stop

service php8.1-fpm restart && service nginx restart && service mysql restart

# INSTALLING MYSQL
echo "*************************"
echo "* UPDATE MYSQL SETTINGS *"
echo "*************************"
mysql_pass=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '$mysql_pass';"
mysql -e "DELETE FROM mysql.user WHERE User='';"
mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
mysql -e "DROP DATABASE IF EXISTS test;"
mysql -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"
mysql -e "FLUSH PRIVILEGES;"
mysql -e "SELECT user,authentication_string,plugin,host FROM mysql.user;"
mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH caching_sha2_password BY '$mysql_pass';"
mysql -u root -p$mysql_pass -e "FLUSH PRIVILEGES;"
mysql -u root -p$mysql_pass -e "SELECT user,authentication_string,plugin,host FROM mysql.user;"

# INSTALLING PHPMYADMIN
echo "******************************"
echo "* UPDATE PHPMYADMIN SETTINGS *"
echo "******************************"
ln -s /usr/share/phpmyadmin /var/www/pmya

# CLEAR USER OWNER TO WEBROOT FOLDER
echo "**************************************"
echo "* CLEAR USER OWNER TO WEBROOT FOLDER *"
echo "**************************************"
chown -R www-data:www-data /var/www/

service php8.1-fpm restart && service nginx restart && service mysql restart

echo "PS1='\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;35m\]\w\[\033[1;31m\]\$\[\033[0m\] '" >> ~/.bashrc

PMA_PATH='http(s)://example.com/pmya'

if [ $DOMAIN_NAME ]
then
    PMA_PATH="http(s)://$DOMAIN_NAME/pmya"
    certbot --nginx -d $DOMAIN_NAME -d www.$DOMAIN_NAME
fi

# PATHS
echo "******************************************************"
echo ""
echo "YOUR WEBROOT PATH: /var/www"
echo "YOUR PHPMYADMIN PATH: $PMA_PATH"
echo "YOUR MYSQL LOGIN: root"
echo "YOUR MYSQL PASSWORD: $mysql_pass"
echo ""
echo "Ending Script in date time : $(date)"
echo ""
echo "******************************************************"

# Server Scripts
## Install Bash Script for Dokerize Laravel on Ubuntu 20.04
```
set .env variable DB_HOST=mysql
set .env variable DB_DATABASE=db_name
set .env variable DB_USERNAME=db_user
set .env variable DB_PASSWORD=db_password
set .env variable QUEUE_CONNECTION=database

curl https://gitlab.com/kem1980/server-scripts/-/raw/master/laravel-doker/laradocker --ssl-no-revoke -o laradocker

sudo bash laradocker up -d
```

## Install Basic Lemp on Ubuntu 20.04
```
curl https://gitlab.com/kem1980/server-scripts/-/raw/master/lemp-basic-ubuntu-20.04.sh --ssl-no-revoke -o lemp.sh
sudo bash lemp.sh
```

## Install Laravel Lemp on Ubuntu 20.04
```
curl https://gitlab.com/kem1980/server-scripts/-/raw/master/lemp-laravel-ubuntu-20.04.sh --ssl-no-revoke -o lemp.sh
sudo bash lemp.sh
```

- На первый вопрос phpMyAdmin нажать ESC
- На второй вопрос phpMyAdmin ответить NO

## Установка SSL сертификата во время установки сервера
- Указать доменное имя когда спросит скрипт
- Ответить на вопросы сертбота

## Установка SSL сертификата во после установки сервера
- Раскоментировать строку ```server_name example.com;``` в конфиге /etc/nginx/sites-enabled/default.conf
- Заменить ```example.com``` на свой домен без WWW
- Перезапустить nginx командой ```service nginx restart```
- Запустить команду ```certbot --nginx -d example.com www.example.com```
- Ответить на вопросы сертбота
